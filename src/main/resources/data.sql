/*admin*/
INSERT INTO user (username, firstname, lastname, password, email) VALUES ('admin', 'Christian', 'Sillaber', '30274c47903bd1bac7633bbf09743149ebab805f', 'info@tempo.at')
INSERT INTO userrole (USER_USERNAME, ROLES) VALUES ('admin', 'ADMIN')
INSERT INTO userrole (USER_USERNAME, ROLES) VALUES ('admin', 'MANAGER')
INSERT INTO userrole (USER_USERNAME, ROLES) VALUES ('admin', 'USER')



/*ps-leiter*/
INSERT INTO user (username, firstname, lastname, password, email) VALUES ('leiter1', 'Francis', 'Underwood', '30274c47903bd1bac7633bbf09743149ebab805f', 'ps-leiter1@tempo.at')
INSERT INTO userrole (USER_USERNAME, ROLES) VALUES ('leiter1', 'MANAGER')
INSERT INTO userrole (USER_USERNAME, ROLES) VALUES ('leiter1', 'USER')


/*students*/
INSERT INTO user (username, firstname, lastname, password, email) VALUES ('student1', 'John', 'Doe', '30274c47903bd1bac7633bbf09743149ebab805f', 'student1@tempo.at')
INSERT INTO userrole (USER_USERNAME, ROLES) VALUES ('student1', 'USER')
INSERT INTO user (username, firstname, lastname, password, email) VALUES ('student2', 'Jane', 'Doe', '30274c47903bd1bac7633bbf09743149ebab805f', 'student2@tempo.at')
INSERT INTO userrole (USER_USERNAME, ROLES) VALUES ('student2', 'USER')
INSERT INTO user (username, firstname, lastname, password, email) VALUES ('student3', 'Max', 'Musterstudent', '30274c47903bd1bac7633bbf09743149ebab805f', 'student3@tempo.at')
INSERT INTO userrole (USER_USERNAME, ROLES) VALUES ('student3', 'USER')

/*projects*/
INSERT INTO project (id, name, description) VALUES (1, 'Demoprojekt', 'Demoprojekt')


/*member*/
INSERT INTO member (username, projectid, role) VALUES ('leiter1', 1, 'LEADER')
INSERT INTO member (username, projectid, role) VALUES ('student1', 1, 'STUDENT')
INSERT INTO member (username, projectid, role) VALUES ('student2', 1, 'STUDENT')
INSERT INTO member (username, projectid, role) VALUES ('student3', 1, 'STUDENT')

/*jobs*/
INSERT INTO job (id, name, description, projectid) VALUES (1, 'GUI', 'Frontend', 1)
INSERT INTO job (id, name, description, projectid) VALUES (2, 'Data', 'Data', 1)
INSERT INTO job (id, name, description, projectid) VALUES (3, 'Controller', 'Program logic', 1)
INSERT INTO job (id, name, description, projectid) VALUES (4, 'Testing', 'JUnit, ...', 1)

/*times*/
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (1, '2017-01-15 00:00:00', '2017-01-15 01:00:00', 'student1', 1)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (2, '2017-01-15 01:00:00', '2017-01-15 01:30:00', 'student1', 2)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (3, '2017-01-15 21:00:00', '2017-01-16 01:00:00', 'student1', 3)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (4, '2017-01-15 01:00:00', '2017-01-15 02:00:00', 'student2', 2)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (5, '2017-01-16 15:00:00', '2017-01-16 22:00:00', 'student3', 1)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (6, '2017-01-19 11:15:00', '2017-01-19 14:21:00', 'student2', 2)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (7, '2017-01-20 09:25:00', '2017-01-20 10:33:00', 'student3', 3)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (8, '2017-01-20 09:30:30', '2017-01-20 11:45:00', 'student2', 4)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (9, '2017-01-20 13:18:00', '2017-01-20 14:55:55', 'student1', 4)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (10, '2017-01-20 21:15:00', '2017-01-20 23:27:00', 'student3', 2)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (11, '2017-01-21 08:30:30', '2017-01-21 10:45:00', 'student2', 3)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (12, '2017-01-21 09:38:00', '2017-01-21 11:24:25', 'student1', 1)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (13, '2017-01-21 13:26:10', '2017-01-21 14:45:00', 'student2', 4)
INSERT INTO time (id, fromtime, totime, username, jobid) VALUES (14, '2017-01-21 14:08:00', '2017-01-21 14:10:00', 'student1', 1)

