package at.qe.sepm.skeleton.configs;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.MimeMappings;
import org.springframework.stereotype.Component;

/**
 * Created by alexanderleonhartsberger-schr on 12.01.17.
 */
@Component
public class ServletCustomizer implements EmbeddedServletContainerCustomizer {

    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {
        MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
        mappings.add("woff","application/font-woff");
        mappings.add("woff2","application/font-woff2");
        mappings.add("eot","application/vnd.ms-fontobject2");
        mappings.add("ttf","application/x-font-ttf");
        container.setMimeMappings(mappings);
    }
}