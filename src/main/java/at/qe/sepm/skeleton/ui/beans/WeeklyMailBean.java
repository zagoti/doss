package at.qe.sepm.skeleton.ui.beans;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import at.qe.sepm.skeleton.model.*;

import at.qe.sepm.skeleton.repositories.MemberRepository;
import at.qe.sepm.skeleton.repositories.ProjectRepository;
import at.qe.sepm.skeleton.repositories.TimeRepository;
import at.qe.sepm.skeleton.services.MailService;

import java.sql.Timestamp;
import java.util.*;

@Component
public class WeeklyMailBean {

	@Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private TimeRepository timeRepository;

    @Scheduled(cron = "0 0 0 * * 0") // run each sunday at 00:00
    public void sendWeeklyReport() {
        List<Project> projects = projectRepository.getAll();
        for (Project project : projects) {
            User leader = memberRepository.getLeaderFromProject(project.getId());
            List<Member> members = new LinkedList();
            members.addAll( project.getMembers() );
            List<Job> jobs = new LinkedList();
            jobs.addAll( project.getJobs() );
            List<User> users = new LinkedList();
            for ( Member member : members )
                users.add( member.getUser() );
            String toMail = "flashfreezer@gmail.com";//leader.getMail();

            Date today = new Date();
            Timestamp lastWeek = new Timestamp(today.getYear(), today.getMonth(), today.getDay() - 7, 0, 0, 0, 0);
            List<Time> times = new LinkedList(timeRepository.getTimesOfUserOfJobsOfLastWeek(jobs, lastWeek));

            Double sum = 0d;
            Map<String,Double> hours = new HashMap();
            for ( Time time : times ) {
                Double hoursOfUser = (double)(time.getToTime().getTime() - time.getFromTime().getTime())/1000d/60d/60d;
                sum += hoursOfUser;
                String key = time.getUser().getFirstName() + " " + time.getUser().getLastName();
                if ( hours.containsKey(key) )
                    hours.put(key, Math.round((hours.get(key) + hoursOfUser) * 100d) / 100d);
                else
                    hours.put(key, Math.round(hoursOfUser * 100d) / 100d);
            }
            sum = Math.round(sum * 100d) / 100d;

            StringBuilder sbmsg = new StringBuilder();
            sbmsg.append("Hallo " + leader.getFirstName() + ",\nes wurdem im Verlauf der letzten Woche ");
            sbmsg.append(sum.toString());
            sbmsg.append(" Stunden in das Projekt \"");
            sbmsg.append(project.getName());
            sbmsg.append("\" investiert.\nDabei haben die einzelnen Mitglieder wie folgt viele Stunden investiert:\n");
            for (Map.Entry<String, Double> hoursEntry : hours.entrySet()) {
                sbmsg.append("- " + hoursEntry.getKey() + ": " + hoursEntry.getValue().toString() + "\n");
            }
            sbmsg.append("\n\nMit freundlichen Grüßen\nTempo Team");

            Date date = new Date();

            MailService.sendMail("Wöchentlicher Report: " + project.getName() + " - " + date.getDay() + "." + date.getMonth() + " " + date.getYear(), sbmsg.toString(), toMail, "tempo@algebros.org", "crater.uberspace.de", "ImrDbSG6Qy9lgaRe");
        }

    }

}
