package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Job;
import at.qe.sepm.skeleton.model.Project;
import at.qe.sepm.skeleton.services.JobService;
import at.qe.sepm.skeleton.services.ProjectService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the job detail view.
 *
 * @author Stephan Eberharter <stephan.eberharter@student.uibk.ac.at>
 * @version 1.0
 */
@Component
@Scope("view")
public class JobCreateController {

    @Autowired
    private JobService jobService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private SessionInfoBean sessionInfo;

    private Job newJob;
    private String project;
    
    public Collection<Project> getPossibleProjects(){
    	if(sessionInfo.hasRole(at.qe.sepm.skeleton.model.UserRole.ADMIN.toString())) {
    		return projectService.getAllProjects();
    	} else {
    		return projectService.getProjectsOfLeader(sessionInfo.getCurrentUserName());
    	}
    }

    public void setNewJob(Job newJob) {
        this.newJob = newJob;
        doReloadNewJob();
    }

    /**
     * Returns the currently displayed job.
     *
     * @return
     */
    public Job getNewJob() {
        if(newJob != null) return newJob;
        else {
            newJob = new Job();
            newJob.setName("Job Name");
            return newJob;
        }
    }

    /**
     * Action to force a reload of the currently displayed job.
     */
    public void doReloadNewJob() {
        newJob = jobService.loadJob(newJob.getId());
    }

    /**
     * Action to save the currently displayed job.
     */
    public void doSaveJob() {
        newJob = this.jobService.saveJob(newJob);
    }

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		newJob.setProject(projectService.getProjectWithName(project));
		this.project = project;
	}

}
