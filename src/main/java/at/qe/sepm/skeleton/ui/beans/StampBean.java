package at.qe.sepm.skeleton.ui.beans;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by alexanderleonhartsberger-schr on 12.01.17.
 */
@ManagedBean
@Scope("session")
public class StampBean {

    @Autowired
    private SessionInfoBean sessionInfoBean;

    @Autowired
    private JobService jobService;

    @Autowired
    private TimeService timeService;

    @Autowired
    private UserService userService;

    private String jobstring;
    private Job job;
    private boolean stampDisabled = true;

    public boolean isNoJobs() {
        return noJobs;
    }

    private boolean noJobs = false;

    public Collection<Job> getAllJobs(){
        Set<Member> memberships = userService.getAuthenticatedUser().getMemberships();
        Project userProject = new Project();
        for (Member mbr : memberships){
           userProject = mbr.getProject();
        }
        if (userProject.getJobs().isEmpty()) noJobs = true;
        return userProject.getJobs();
    }

    public void onJobChange(){
    }

    public String getJob() {
        return this.jobstring;
    }


    public void setJob(String job) {

        this.jobstring = job;
        if(job.equals("0")){
            setStampDisabled(true);
        } else {
            this.job = jobService.getJobByName(jobstring);
            setStampDisabled(false);
        }
    }

    public void setStampDisabled (boolean value) {
        this.stampDisabled = value;
    }

    public boolean getStampDisabled(){
        return stampDisabled;
    }


    public String getCurrentUserRealName(){
        return sessionInfoBean.getCurrentUserRealName();
    }

    public boolean isStamped(){
        return timeService.isStampedIn();
    }

    public String getButtonText() {
        if(isStamped()) return "Ausstempeln";
        else return "Einstempeln";
    }

    public void stamp() {

        boolean oldValue = isStamped();
        timeService.stamp(job);
        FacesContext context = FacesContext.getCurrentInstance();
        if (oldValue != isStamped()) context.addMessage("Eingestempelt", new FacesMessage("Stempelvorgang erfolgreich"));
        else System.out.println("not stamped");
    }

}
