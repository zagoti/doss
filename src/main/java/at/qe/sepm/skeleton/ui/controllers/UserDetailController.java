package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.EnumSet;
import java.util.Set;

/**
 * Controller for the user detail view.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@Component
@Scope("session")
public class UserDetailController {

    @Autowired
    private UserService userService;

    @Autowired
    private SessionInfoBean session;

    /**
     * Attribute to cache the currently displayed user
     */
    private User user;

    private String errorMessage;

    public String getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(String selectedRole) {
        this.selectedRole = selectedRole;
    }

    private String selectedRole;

    /**
     * Sets the currently displayed user and reloads it form db. This user is
     * targeted by any further calls of
     * {@link #doReloadUser()}, {@link #doSaveUser()} and
     * {@link #doDeleteUser()}.
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
        doReloadUser();
    }

    /**
     * Returns the currently displayed user.
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    public String getErrorMessage() {
System.out.println("getting error-message: " + errorMessage);
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Action to force a reload of the currently displayed user.
     */
    public void doReloadUser() {
        user = userService.loadUser(user.getUserName());
        Set<UserRole> roles = user.getRoles();
        if (roles.size() > 2) selectedRole = "Admin";
        else if (roles.size() > 1) selectedRole = "Leiter";
        else selectedRole = "Student";
    }

    /**
     * Action to save the currently displayed user.
     */
    public void doSaveUser() {
        Set<UserRole> roles = EnumSet.of(UserRole.USER);
        roles.add(UserRole.USER); //it is always a user.
        if(selectedRole.equals("Leiter")) roles.add(UserRole.MANAGER);
        if(selectedRole.equals("Admin")) {
            roles.add(UserRole.MANAGER);
            roles.add(UserRole.ADMIN);
        }
        user.setRoles(roles);
        user = this.userService.saveUser(user);
    }

    /**
     * Action to delete the currently displayed user.
     */
    public void doDeleteUser() {
//System.out.println(errorMessage);
        if (user.getRoles().contains(userService.getRole("ADMIN")) && (user.getUserName() == session.getCurrentUserName())) {
//System.out.println(session.getCurrentUserName());
//System.out.println(user);
            setErrorMessage("Error: If you delete yourself, do you actually exist?");
            user = null;
        } else {
            setErrorMessage("");
            this.userService.deleteUser(user);
        }
    }

}
