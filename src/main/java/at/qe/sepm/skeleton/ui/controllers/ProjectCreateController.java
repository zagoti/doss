package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Project;
import at.qe.sepm.skeleton.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the project detail view.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@Component
@Scope("view")
public class ProjectCreateController {

    @Autowired
    private ProjectService projectService;

    private Project newProject;
/* never called
    public void setNewProject(Project newProject) {
        this.newProject = newProject;
        doReloadNewProject();
    }
*/
    public void setProject(Project newProject) {
System.out.println("add");
System.out.println(newProject);
    }

    /**
     * Returns the currently displayed project.
     *
     * @return
     */
    public Project getNewProject() {
        if(newProject != null) return newProject;
        else {
            newProject = new Project();
            newProject.setName("Project Name");
            newProject.setDescription("Project Description");
            return newProject;
        }
    }

    /**
     * Action to force a reload of the currently displayed project.
     */
    public void doReloadNewProject() {
        newProject = projectService.loadProject(newProject.getName());
    }

    /**
     * Action to save the currently displayed project.
     */
    public void doSaveProject() {
        newProject = this.projectService.saveProject(newProject);
        newProject = new Project();
        newProject.setName("Project Name");
        newProject.setDescription("Project Description");
    }

}
