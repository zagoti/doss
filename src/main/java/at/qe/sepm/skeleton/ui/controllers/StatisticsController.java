package at.qe.sepm.skeleton.ui.controllers;

import java.util.*;
import java.sql.Timestamp;

import org.primefaces.model.chart.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Project;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.MemberService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

/**
 * Controller for the statistics menu.
 *
 * @author Stephan Eberharter <stephan.eberharter@student.uibk.ac.at>
 * @version 1.0
 */
@Component
@Scope("session") // which scope?
public class StatisticsController {

	@Autowired
	private ChartController chartController;
	@Autowired
	private MemberService memberService;
	@Autowired
	private SessionInfoBean sessionInfo;

	// information for menu
	/** Stores the menu entries (which get filled after instantiation of the class) */
	private List<SelectItem> categories;
	/** From the menu selected item. */
	private String selection;

	// from the calendar selected values
	/** Date from which one the statistics should be counted */
	private Date from;
	/** Date to which one the statistics should be counted (excluding this one) */
	private Date to;

	// charts part
	/** Whether there are charts that should be shown or not. Default false, since no information is provided. */
	private boolean showCharts = false; // before the user selected a team no
										// charts should be shown
	/** First pie chart. Stores information about hours spent by a user on jobs or by a project on members. */
	private PieChartModel pieModel1; // shows project jobs
	/** Second pie chart. Stores information about hours spent by a project on jobs. */
	private PieChartModel pieModel2; // shows user/project members time
	private PieChartModel pieModel3;
	/** Bar chart that represents the hours spent by a user/project per day. */
	private BarChartModel barModel; // show user/project daily progress


	@PostConstruct
	public void init() {
		categories = new LinkedList<SelectItem>();
		for (Project p : memberService.getLeaderProjects(sessionInfo.getCurrentUserName())) {
			SelectItemGroup tmpGroup = new SelectItemGroup(p.getName());
			List<SelectItem> items = new LinkedList<SelectItem>();
			items.add(new SelectItem(p.getId().toString() + ",null", "Overview")); // get
																					// project
																					// details
			for (User u : memberService.getProjectStudents(p.getId())) {
				items.add(new SelectItem(p.getId().toString() + "," + u.getUserName(), u.getUserName())); // get
																											// member
																											// details
			}
			tmpGroup.setSelectItems(items.toArray(new SelectItem[items.size()]));
			categories.add(tmpGroup);
		}

	}



	/** 
	 * Sends the information entered in the menu to ChartController to get the models.
	 * @return returns "Success" if all information provided or "Please enter a time frame" if the user didn't specify the time frame
	 */
	public String buttonAction() {
		if (to == null || from == null) {
			/* if we want to get statistics from the header */
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.HOUR_OF_DAY, 0);
			to = cal.getTime();
			cal.add(Calendar.WEEK_OF_MONTH, -2); // to get previous year add -1
			from = cal.getTime();
			System.out.println(to.toString() + from.toString());
		}
		
		showCharts = true;
		System.out.println("Selection: " + selection);
		// very ugly hack to save as string
		List<String> parameters = Arrays.asList(selection.split(","));
		Long projectId = new Long(parameters.get(0));
		String username = parameters.get(1);
		if (username.equals("null")) {
			pieModel1 = chartController.getProjectMembersPie(projectId, new Timestamp(from.getTime()),
					new Timestamp(to.getTime()), "");
			barModel = chartController.getProjectDailyHoursBar(projectId, new Timestamp(from.getTime()),
					new Timestamp(to.getTime()), "");
		} else {
			pieModel1 = chartController.getUserJobsPie(username, new Timestamp(from.getTime()),
					new Timestamp(to.getTime()), "");
			barModel = chartController.getUserDailyHoursBar(username, new Timestamp(from.getTime()),
					new Timestamp(to.getTime()), "");
		}
		pieModel2 = chartController.getProjectJobsPie(projectId, new Timestamp(from.getTime()),
				new Timestamp(to.getTime()), "");
		pieModel3 = chartController.getProjectMembersPie(projectId, new Timestamp(from.getTime()),
				new Timestamp(to.getTime()), "");
		pieModel3.setSeriesColors("0081D5,03BD5B,D1335B,9DBF15,FF9947,A939B9,374355");
		pieModel1.setSeriesColors("0081D5,03BD5B,D1335B,9DBF15,FF9947,A939B9,374355");
		pieModel2.setSeriesColors("0081D5,03BD5B,D1335B,9DBF15,FF9947,A939B9,374355");
		barModel.setSeriesColors("0081D5,03BD5B,D1335B,9DBF15,FF9947,A939B9,374355");
		return "Success";
	}

	public List<SelectItem> getCategories() {
		return categories;
	}

	public String getSelection() {
		return buttonAction();
	}

	public void setSelection(String selection) {
		System.out.println(selection);
		this.selection = selection;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public PieChartModel getPieModel1() {

		return pieModel1;
	}

	public PieChartModel getPieModel3() {
		return pieModel3;
	}
	
	public PieChartModel getPieModel2() {
		return pieModel2;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public boolean isShowCharts() {
		return showCharts;
	}

}
