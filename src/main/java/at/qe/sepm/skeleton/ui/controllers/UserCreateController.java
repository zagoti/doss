package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.MailService;
import at.qe.sepm.skeleton.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.ManagedBean;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

/**
 * Controller for the newUser detail view.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@Component
@Scope("view")
@ManagedBean
public class UserCreateController {

    @Autowired
    private UserService userService;

    /**
     * Attribute to cache the currently displayed newUser
     */
    private User newUser;

    public String getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(String selectedRole) {
        this.selectedRole = selectedRole;
    }

    private String selectedRole;
/* never used
    public void setNewUser(User newUser) {
System.out.println("new");
System.out.println(newUser);
        this.newUser = newUser;
        doReloadNewUser();
    }
*/
    public void setUser(User newUser) {
System.out.println("added User: " + newUser);
    }

    public void doReloadNewUser() {
        newUser = userService.loadUser(newUser.getUserName());
        selectedRole = "";
    }


    /**
     * Returns the currently displayed newUser.
     *
     * @return
     */
    public User getNewUser() {
        selectedRole = "";
        if(newUser == null) {
            newUser = new User();
            newUser.setUserName("newUser");
        }
        return newUser;
    }

    /**
     * Action to save the currently displayed newUser.
     */
    public void doSaveNewUser() {
        StringBuilder sbmsg = new StringBuilder();
        sbmsg.append("Hallo " + newUser.getFirstName() + "\n");
        sbmsg.append("\n Es wurde ein neuer Account für dich erstellt, hier sind deine Zugangsdaten: \n");
        sbmsg.append("Benutzername: " + newUser.getUserName() + "\n");
        sbmsg.append("Passwort: " + newUser.getPassword() + "\n");
        sbmsg.append("\n\nViel Spaß im Proseminar, arbeite fleißig\n");
        sbmsg.append("\n\nMit freundlichen Grüßen\nTempo Team");
        MailService.sendMail("Benutzerkonto wurde erstellt", sbmsg.toString(), newUser.getEmail());

        Set<UserRole> roles = EnumSet.of(UserRole.USER);
        roles.add(UserRole.USER); //it is always a user.
        if(selectedRole.equals("Leiter")) roles.add(UserRole.MANAGER);
        if(selectedRole.equals("Admin")) {
            roles.add(UserRole.MANAGER);
            roles.add(UserRole.ADMIN);
        }
        newUser.setRoles(roles);
        newUser = this.userService.saveUser(newUser);
    }

}
