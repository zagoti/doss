package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.util.*;

/**
 * Controller for the project detail view.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@Component
@Scope("view")
public class ProjectDetailController {

    @Autowired
    private ProjectService projectService;

    private Project project;

    public void setProject(Project project) {
        this.project = project;
        doReloadProject();
    }

    public Project getProject() {
        return project;
    }

    /**
     * Action to force a reload of the currently displayed project.
     */
    public void doReloadProject() {
        project = projectService.loadProject(project.getName());
    }

    /**
     * Action to save the currently displayed project.
     */
    public void doSaveProject() {
        project = this.projectService.saveProject(project);
    }

    /**
     * Action to delete the currently displayed project.
     */
    public void doDeleteProject() {
        this.projectService.deleteProject(project);
        project = null;
    }

    /**
     * Returns a collection of all members
     */
    public Collection<Member> getMembersOfProject() {
        return projectService.getMembersOfProject(project);
    }

}
