package at.qe.sepm.skeleton.ui.beans;

import at.qe.sepm.skeleton.model.Time;
import at.qe.sepm.skeleton.model.Job;
import at.qe.sepm.skeleton.services.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by alexanderleonhartsberger-schr on 13.01.17.
 */
@Component
@Scope("request")
public class StudentTimeBean {

    @Autowired
    TimeService timeService;

    public Collection<Time> getAllTimes(){
        return timeService.getTimes();
    }

    public String getTotalTime(){
        Collection<Time> times = getAllTimes();
        long totalTime = 0;
        for (Time oneTime : times) {
            if (oneTime.getToTime() != null ) {
                long interval = oneTime.getToTime().getTime() - oneTime.getFromTime().getTime();
                totalTime += interval;
            }
        }
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(totalTime),
                TimeUnit.MILLISECONDS.toMinutes(totalTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalTime)),
                TimeUnit.MILLISECONDS.toSeconds(totalTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime)));
        return hms;
    }

    public class TimesListItem {

        private String date;
        private String duration;
        private String job = "<none>";

        public String getDuration() {
            return duration;
        }

        public String getDate() {
            return date;
        }

        public String getJob() {
            return job;
        }

    }

    public List getTimesList(){
//System.out.println("this is called");
        Collection<Time> times = getAllTimes();
        List<TimesListItem> list = new LinkedList<TimesListItem>();
        for (Time oneTime : times) {
            if (oneTime.getToTime() != null ) {
                String fromDate = oneTime.getFromTime().toString().substring(0,11);
                long duration = (oneTime.getToTime().getTime() - oneTime.getFromTime().getTime()) / 1000;
                long h = duration / 3600;
                long m = (duration - h * 3600) / 60;
                long s = duration - h * 3600 - m * 60;
                TimesListItem item = new TimesListItem();
                item.date = fromDate;
                item.duration = String.format("%02d", h) + ":" + String.format("%02d", m) + ":" + String.format("%02d", s);
                item.job = oneTime.getJob().getName();
                list.add(item);
            }
        }
        return list;
    }

}
