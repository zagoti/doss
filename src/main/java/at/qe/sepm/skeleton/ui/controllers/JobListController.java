package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Job;
import at.qe.sepm.skeleton.model.Project;
import at.qe.sepm.skeleton.services.JobService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Controller for the user list view.
 *
 * @author Stephan Eberharter <stephan.eberharter@student.uibk.ac.at>
 * @version 1.0
 */
@Component
@Scope("view")
public class JobListController {

    @Autowired
    private JobService jobService;
    @Autowired
    private JobDetailController jobDetail;

    /**
     * Returns a list of all jobs.
     *
     * @return
     */
    public Collection<Job> getJobs() {
    	Collection<Job> jobs = new LinkedList<Job>();
    	for(Project project: jobDetail.getPossibleProjects()) {
    		jobs.addAll(jobService.getJobByProject(project));
    	}
        return jobs;
    }

}
