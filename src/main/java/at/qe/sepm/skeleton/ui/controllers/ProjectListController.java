package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Project;
import at.qe.sepm.skeleton.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Controller for the user list view.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@Component
@Scope("view")
public class ProjectListController {

    @Autowired
    private ProjectService projectService;

    /**
     * Returns a list of all projects.
     *
     * @return
     */
    public Collection<Project> getProjects() {
        return projectService.getAllProjects();
    }

}
