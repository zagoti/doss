package at.qe.sepm.skeleton.ui.controllers;

import java.sql.Timestamp;
import java.util.*;

import org.primefaces.model.chart.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.services.StatisticsService;

/**
 * Generate the requested chart.
 *
 * @author Stephan Eberharter <stephan.eberharter@student.uibk.ac.at>
 * @version 1.0
 */
@Component
@Scope("view") //which scope?
public class ChartController {
	
	@Autowired
	private StatisticsService statisticsService;
	
	/**
	 * Creation of the graphical model as a pie
	 * @param values data that should be represented
	 * @param title title of the graph
	 * @return Graphical representation of the data. category is the key and data is the value.
	 */
	private PieChartModel createPieModel(Map<String,Integer> values, String title) {
		PieChartModel model = new PieChartModel();
		
		//set values
		for(String i: values.keySet()) {
			model.set(i, values.get(i)/3600.D); //display in hours (values have integer seconds)
		}
		
		model.setLegendPosition("sw");
		model.setShowDataLabels(true);
		model.setTitle(title);
		model.setDatatipFormat("%s %.2f");
		return model;
	}
	
	
	// creates model with amount of hours spent each day
	private BarChartModel createDateModel(Map<String, Integer> barValues, String title) {
		BarChartModel model = new BarChartModel();
		
		//sort dates
		Comparator<String> compareDates = new Comparator<String>() { 
				public int compare(String s1, String s2) {
					return s1.compareTo(s2);
				};
		};
		List<String> keys =  new LinkedList<String>();
		keys.addAll(barValues.keySet());
		keys.sort(compareDates);
		
		
		//create the bar
		int maxYValue = 0;
		ChartSeries c = new ChartSeries();
		for(String i: keys) {
			Double tmp = barValues.get(i)/3600.D; //display in hours (values has integer seconds)
			c.set(i, tmp);
			if(maxYValue < tmp+1) { //precision not that important
				maxYValue = tmp.intValue()+1;
			}
		}
		model.addSeries(c);
		
		model.setTitle(title);
        model.getAxis(AxisType.Y).setLabel("Hours");
        model.getAxis(AxisType.Y).setMax(maxYValue);
        model.getAxis(AxisType.Y).setMin(0);
        model.getAxis(AxisType.X).setTickAngle(-90);
        model.setDatatipFormat("%s %.2f");
         
        return model;
	}
	
	//add 1 day to "to" so it includes the selected day in the statistics
	private Timestamp addOneDay(Timestamp date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);	
		c.add(Calendar.DATE, 1);
		return new Timestamp(c.getTime().getTime());
	}
	
	
	//Project: job and the seconds spent on them
	public PieChartModel getProjectJobsPie(Long projectId, Timestamp from, Timestamp to, String title) {
		return createPieModel(statisticsService.getProjectJobsData(projectId, from, addOneDay(to)), title);
	}
	
	//Project: members and their seconds spent
	public PieChartModel getProjectMembersPie(Long projectId, Timestamp from, Timestamp to, String title) {
		return createPieModel(statisticsService.getProjectMembersData(projectId, from, addOneDay(to)), title);
	}
	
	//User: seconds spent on jobs
	public PieChartModel getUserJobsPie(String username, Timestamp from, Timestamp to, String title) {
		return createPieModel(statisticsService.getUserJobsData(username, from, addOneDay(to)), title);
	}
	
	//Project: seconds the project spent on each day
	public BarChartModel getProjectDailyHoursBar(Long id, Timestamp from, Timestamp to, String title) {
		Map<String, Integer> barValues = new HashMap<String, Integer>();
		Calendar c = Calendar.getInstance();	//calendar to count days
		c.setTime(from);
		Timestamp correctTo = addOneDay(to);
		//add entry for every day
		for(; c.getTime().before(correctTo);) {
			//set the time for the corresponding day
			Timestamp from1 = new Timestamp(c.getTime().getTime());
			String key = new Timestamp(c.getTime().getTime()).toString().substring(0, 11);
			c.add(Calendar.DATE, 1);
			Timestamp to1 = new Timestamp(c.getTime().getTime());
            barValues.put(key, statisticsService.getProjectSecondsData(id, from1, to1));
            
		}
		return createDateModel(barValues, title);
	}
	
	//User: seconds the user spent on each day
	public BarChartModel getUserDailyHoursBar(String id, Timestamp from, Timestamp to, String title) {
		Map<String, Integer> barValues = new HashMap<String, Integer>();
		Calendar c = Calendar.getInstance();	//calendar to count days
		c.setTime(from);
		//add entry for every day
		Timestamp correctTo = addOneDay(to);
		for(; c.getTime().before(correctTo);) {
			//set the time for the corresponding day
			Timestamp from1 = new Timestamp(c.getTime().getTime());
			String key = new Timestamp(c.getTime().getTime()).toString().substring(0, 11);
			c.add(Calendar.DATE, 1);
			Timestamp to1 = new Timestamp(c.getTime().getTime());
            barValues.put(key, statisticsService.getUserSecondsData(id, from1, to1));
		}
		return createDateModel(barValues, title);
	}
}
