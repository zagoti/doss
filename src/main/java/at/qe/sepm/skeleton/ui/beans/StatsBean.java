package at.qe.sepm.skeleton.ui.beans;

import at.qe.sepm.skeleton.model.Project;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.ProjectService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.controllers.StatisticsController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.Collection;

/**
 * Created by alexander on 15.01.17.
 */
@ManagedBean
@Scope("session")
public class StatsBean {

    @Autowired
    ProjectService projectService;
    @Autowired
    UserService userService;
    StatisticsController statisticsController;
    @Autowired
    public StatsBean(StatisticsController statisticsController){
        this.statisticsController=statisticsController;
    }
    @Autowired
    private SessionInfoBean sessionInfo;

    private String project;
    private String student;

    public Collection<Project> getAllProjects(){
        return projectService.getAllProjects();
    }
    
    public Collection<Project> getProjectsOfLeader(){
        return projectService.getProjectsOfLeader(sessionInfo.getCurrentUserName());
    }

    public Collection<User> getCorrespondingStudents(){
            return userService.getAllUsersFromProject(project);
    }


    public String getProject() {
        return this.project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getStudent(){
        return this.student;
    }

    public void setStudent(String student){
        this.student = student;
    }

    public void onProjectChange(){
        userService.getAllUsersFromProject(project);
    }

    public void onStudentChange(){
        statisticsController.setSelection(projectService.getProjectWithName(project).getId() +","+student);
        statisticsController.buttonAction();
    }

}
