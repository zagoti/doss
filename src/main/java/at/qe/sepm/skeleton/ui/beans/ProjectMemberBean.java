package at.qe.sepm.skeleton.ui.beans;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.*;

import java.util.*;

import org.springframework.context.annotation.Scope;
import javax.annotation.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;

@ManagedBean
@Scope("session")
public class ProjectMemberBean {

	@Autowired
    ProjectService projectService;

    @Autowired
    MemberService memberService;

    @Autowired
    UserService userService;

	private String projectName;
    private Project project;
    private String memberName;
	private User member;
    private String roleName;
	private MemberRole role;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
        this.project = projectService.getProjectWithName(projectName);
		this.projectName = projectName;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
//System.out.println("set member to: " + memberName);
        this.member = userService.loadUser(memberName);
		this.memberName = memberName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
        role = memberService.getRole(roleName);
		this.roleName = roleName;
	}

    /**
     * Returns a collection of all projects
     */
    public Collection<String> getAllProjectNames() {
        return projectService.getAllProjectNames();
    }

    /**
     * Returns a collection of all possible new members (users, which are not already members of this project)
     */
    public Collection<User> getAllPossibleNewMembers() {
        return projectService.getAllPossibleNewMembers(project);
    }

    /**
     * Returns a collection of all members of the selected project
     */
    public Collection<Member> getAllMembers() {
//System.out.println("members from project: " + project);
        if (project != null)
            return projectService.getMembersOfProject(project);
        else
            return null;
    }

    /**
     * Returns a collection of all possbile roles
     */
    public Collection<MemberRole> getAllRoles() {
        return projectService.getAllRoles();
    }

    public void doAddMemberToProject() {
        Member m = new Member();
        m.setUser(member);
        m.setProject(project);

        Set<UserRole> roles = member.getRoles();
        if (roles.size() > 1) m.setRole(MemberRole.LEADER);
        else m.setRole(MemberRole.STUDENT);

        //user can only be in one project
        FacesContext context = FacesContext.getCurrentInstance();
        if(m.getRole() == MemberRole.LEADER || (member.getMemberships().size() == 0)){
            memberService.save(m);
            context.addMessage("Success", new FacesMessage("The user is now assigned to the project"));
        }
        else {
            context.addMessage("Error", new FacesMessage("This user is already in a project"));
        }

        projectName = "";
        memberName = "";
        roleName = "";
        project = null;
        member = null;
        role = null;
    }

    public void doRemoveMemberFromProject() {
//System.out.println("old member: " + member);
        List<Member> ms = memberService.getMemberByNameInProject(member.getUserName(), project);
        for (Member m : ms)
            memberService.delete(m);
        projectName = "";
        memberName = "";
        project = null;
        member = null;
    }

    public void onProjectUpdate() {
//System.out.println("role: " + roleName + " user: " + member + ", project: " + project);
        getAllPossibleNewMembers();
        getAllMembers();
    }

}
