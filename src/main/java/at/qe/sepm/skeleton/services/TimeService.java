package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Job;
import at.qe.sepm.skeleton.model.Time;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.repositories.TimeRepository;
import at.qe.sepm.skeleton.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by mlex on 02.01.2017.
 */
@Component
@Scope("application")
public class TimeService {
    @Autowired
    private TimeRepository TimeRepository;

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('ADMIN')")
    public Collection<Time> getAllTimes() {
        return TimeRepository.findAll();
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public Time saveTime(Time Time) {
        return TimeRepository.save(Time);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteTime(Time Time) {
        TimeRepository.delete(Time);
    }

    public Collection<Time> getTimes() {
        return TimeRepository.getTimesForUser(userService.getAuthenticatedUser().getUserName());
    }

    public boolean isStampedIn() {
        Time lastTime = getLastTime();
        if (lastTime == null) return false;
        if (lastTime.getToTime() != null) return false;
        else return true;
    }

    public void stamp() {
        if (isStampedIn()) {
            //add now to end
            Time lastTime = getLastTime();
            lastTime.setToTime(new Timestamp(System.currentTimeMillis()));
            saveTime(lastTime);
        } else {
            //create new time with now as a start point
            Time newTime = new Time();
            newTime.setUser(userService.getAuthenticatedUser());
            newTime.setFromTime(new Timestamp(System.currentTimeMillis()));
            saveTime(newTime);
        }
    }

    public void stamp(Job job) {
        if (isStampedIn()) {
            //add now to end
            Time lastTime = getLastTime();
            lastTime.setToTime(new Timestamp(System.currentTimeMillis()));
            saveTime(lastTime);
        } else {
            //create new time with now as a start point
            Time newTime = new Time();
            newTime.setJob(job);
            newTime.setUser(userService.getAuthenticatedUser());
            newTime.setFromTime(new Timestamp(System.currentTimeMillis()));
System.out.println("job: " + job);
            saveTime(newTime);
        }
    }

    private Time getLastTime() {
        ArrayList<Time> allTimes = new ArrayList(getTimes());
        if(allTimes.size() == 0) return null;
        else return allTimes.get(allTimes.size() - 1);
    }


}
