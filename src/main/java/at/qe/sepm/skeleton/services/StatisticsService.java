package at.qe.sepm.skeleton.services;


import java.util.*;
import java.sql.Timestamp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.repositories.*;
import at.qe.sepm.skeleton.model.*;



/**
 * Service for accessing data used in Controller.
 *
 * @author Stephan Eberharter <stephan.eberharter@student.uibk.ac.at>
 * @version 1.0
 */
@Component
@Scope("application")
public class StatisticsService {
	
	
	@Autowired
	private JobRepository jobRepo; //get members/jobs from a project
	@Autowired
	private MemberRepository memberRepo; //get memberships from a user
	@Autowired
	private TimeRepository timeRepo;  //get time for jobs/members
	
	//return the jobs of a project and how much got worked on them
	@PreAuthorize("hasAuthority('USER')")
	public Map<String, Integer> getProjectJobsData(Long id, Timestamp from, Timestamp to) {
		Map<String, Integer> rtn = new HashMap<String, Integer>();
		
		Collection<Job> keys = jobRepo.findByProject(id);
		for(Job i: keys) {
			int s = 0;
			for(Time j : timeRepo.findJobTimes(i.getId(), from, to)) {
				s += j.getToTime().getTime()-j.getFromTime().getTime(); 	//add ms for every relevant time-entry
			}
			rtn.put(i.getName(), s/1000);
		}
		
		return rtn;
	}
	
	//return the members of a project and how much they worked
	@PreAuthorize("hasAuthority('MANAGER')") //only leader should see individual stats
	public Map<String, Integer> getProjectMembersData(Long id, Timestamp from, Timestamp to) {
		Map<String, Integer> rtn = new HashMap<String, Integer>();
		
		List<Member> keys = memberRepo.findByProject(id);
		for(Member i: keys) {
			if(i.getRole().equals(at.qe.sepm.skeleton.model.MemberRole.STUDENT)) {	//only students work on the project
				int s = 0;
				for(Time j : timeRepo.findUserTimes(i.getUser().getUserName(), from, to)) {
					s += j.getToTime().getTime()-j.getFromTime().getTime(); 	//add ms for every relevant time-entry
				}
				rtn.put(i.getUser().getUserName(), s/1000);
			}
		}
		return rtn;
	}
	
	//return the jobs from the users project and how much the user worked on them
	@PreAuthorize("hasAuthority('USER')")
	public Map<String, Integer> getUserJobsData(String username, Timestamp from, Timestamp to) {
		Map<String, Integer> rtn = new HashMap<String, Integer>();
		for(Member mid: memberRepo.findByUser(username)) {
			Collection<Job> keys = jobRepo.findByProject(mid.getProject().getId());
			for(Job i: keys) {
				int s = 0;
				for(Time j : timeRepo.findUserJobTimes(username, i.getId(), from, to)) {
					s += j.getToTime().getTime()-j.getFromTime().getTime(); 	//add ms for every relevant time-entry
				}
				rtn.put(i.getName(), s/1000);
			}
		}
		return rtn;
	}
	
	//return all seconds a project worked
	@PreAuthorize("hasAuthority('USER')")
	public Integer getProjectSecondsData(Long id, Timestamp from, Timestamp to) {
		Collection<Job> keys = jobRepo.findByProject(id);
		int rtn = 0;
		for(Job i: keys) {
			for(Time j : timeRepo.findJobTimes(i.getId(), from, to)) {
				rtn += j.getToTime().getTime()-j.getFromTime().getTime(); 	//add ms for every relevant time-entry
			}
		}
		return rtn/1000;
	}
	
	//return all seconds a user worked
	@PreAuthorize("hasAuthority('USER')")
	public Integer getUserSecondsData(String username, Timestamp from, Timestamp to) {
		int rtn = 0;
		for(Time j : timeRepo.findUserTimes(username, from, to)) {
			rtn += j.getToTime().getTime()-j.getFromTime().getTime(); 	//add ms for every relevant time-entry
		}
		return rtn/1000;
	}

}
