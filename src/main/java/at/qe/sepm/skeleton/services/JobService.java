package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.repositories.JobRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by mlex on 02.01.2017.
 */
@Component
@Scope("application")
public class JobService {
    @Autowired
    private JobRepository jobRepository;

    public Job getJobByName(String name) {
        return jobRepository.getJobByName(name);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public Collection<Job> getAllJobs() {
        return jobRepository.findAll();
    }

    @PreAuthorize("hasAuthority('USER')")
    public Job loadJob(Long id) {
        return jobRepository.getJobById(id);
    }

    @PreAuthorize("hasAuthority('USER')")
    public Job saveJob(Job job) {
        return jobRepository.save(job);
    }

    @PreAuthorize("hasAuthority('USER')")
    public void deleteJob(Job job) {
        jobRepository.delete(job);
    }
    
    @PreAuthorize("hasAuthority('USER')")
    public Collection<Job> getJobByProject(Project project) {
    	return jobRepository.findByProject(project.getId());
    }
}
