package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.repositories.*;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

/**
 * Service for member data.
 */
@Component
@Scope("application") //which scope?
public class MemberService {
	
	@Autowired
	private MemberRepository memberRepository;

	public MemberRole getRole(String role) {
		return Enum.valueOf(MemberRole.class, role);
	}

	@PreAuthorize("hasAuthority('MANAGER')")
	public Member save(Member member) {
		return memberRepository.save(member);
	}

	@PreAuthorize("hasAuthority('MANAGER')")
	public void delete(Member member) {
		memberRepository.delete(member);
	}

	@PreAuthorize("hasAuthority('USER')")
	public List<Member> getMemberByName(String name) {
		return memberRepository.findByUser(name);
	}

	@PreAuthorize("hasAuthority('USER')")
	public List<Member> getMemberByNameInProject(String name, Project project) {
		return memberRepository.findByUserInProject(name, project.getId());
	}

	//projects where username is the project leader
    @PreAuthorize("hasAuthority('MANAGER')")
	public List<Project> getLeaderProjects(String username) {
    	List<Project> rtn = new LinkedList<Project>();
		for(Member i: memberRepository.findByUser(username)) {
			if(i.getRole().equals(at.qe.sepm.skeleton.model.MemberRole.LEADER)) {
				rtn.add(i.getProject());
			}
		}
        return rtn;
	}
	
	//list of all students in the project
	@PreAuthorize("hasAuthority('USER')")
	public List<User> getProjectStudents(Long projectId) {
		List<User> rtn = new LinkedList<User>();
		for(Member i: memberRepository.findByProject(projectId)) {
			if(i.getRole().equals(at.qe.sepm.skeleton.model.MemberRole.STUDENT)) {
				rtn.add(i.getUser());
			}
		}
        return rtn;
	}
	
}
