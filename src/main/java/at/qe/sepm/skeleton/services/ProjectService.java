package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.repositories.MemberRepository;
import at.qe.sepm.skeleton.repositories.ProjectRepository;
import at.qe.sepm.skeleton.repositories.TimeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by mlex on 02.01.2017.
 */
@Component
@Scope("application")
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private TimeRepository timeRepository;

    @PreAuthorize("hasAuthority('MANAGER')")
    public Collection<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    public Collection<String> getAllProjectNames() {
        return projectRepository.getAllNames();
    }

    public Project getProjectWithName(String project) {
        return projectRepository.findByName(project);
    }

    @PreAuthorize("hasAuthority('USER')")
    public Collection<Project> getProjectsOfLeader(String username) {
        return projectRepository.getProjectsOfLeader(username);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public Project loadProject(String name) {
        return projectRepository.findByName(name);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public Project saveProject(Project project) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (getProjectWithName(project.getName()) == null) {
            context.addMessage("Success", new FacesMessage("Projekt gespeichert"));
            return projectRepository.save(project);
        } else {
            context.addMessage("Error", new FacesMessage("Fehler: Projekt konnte nicht gespeichert werden, da bereits ein Projekt mit diesem Namen existiert!"));
            return null;
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteProject(Project project) {
        projectRepository.delete(project);
    }

    public Collection<MemberRole> getAllRoles() {
        Collection<MemberRole> roles = new ArrayList<MemberRole>(Arrays.asList(MemberRole.values()));
        return roles;
    }

    public Collection<User> getAllPossibleNewMembers(Project project) {
        return projectRepository.getAllPossibleNewMembers(project);
    }

    public Collection<Member> getMembersOfProject(Project project) {
        return project.getMembers();
    }

}
