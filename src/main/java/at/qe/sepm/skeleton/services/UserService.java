package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.repositories.*;

import java.sql.Timestamp;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 * Service for accessing and manipulating user data.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@Component
@Scope("application")
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserRole getRole(String role) {
        return Enum.valueOf(UserRole.class, role);
    }

    /**
     * Returns a collection of all users.
     *
     * @return
     */
    @PreAuthorize("hasAuthority('MANAGER')")
    public Collection<User> getAllUsers() {
        return userRepository.findAll();
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    public Collection<User> getAllUsersFromProject(String projectName) {
        Collection<User> users = userRepository.findAll();
        Collection<User> usersFromProject = new ArrayList<>();
        for(User user : users){
            Set<Member> memberships = user.getMemberships();
            for (Member msh : memberships){
                if (msh.getProject().getName().equals(projectName)){
                    if (msh.getRole().equals(MemberRole.STUDENT)) {
                        usersFromProject.add(user);
                    }
                }
            }
        }
        return usersFromProject;
    }

    public String getUserRealName(String username){
        User user = userRepository.findFirstByUsername(username);
        return user.getFirstName() + " " + user.getLastName();
    }
    /**
     * Loads a single user identified by its username.
     *
     * @param username the username to search for
     * @return the user with the given username
     */
    @PreAuthorize("hasAuthority('ADMIN') or principal.username eq #username")
    public User loadUser(String username) {
        return userRepository.findFirstByUsername(username);
    }

    /**
     * Saves the user.
     *
     * @param user the user to save
     * @return the updated user
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    public User saveUser(User user) {
        // crypt password
        user.setPassword(new ShaPasswordEncoder().encodePassword(user.getPassword(), null));
        if (user.getRoles() == null)
            user.setRoles(EnumSet.of(UserRole.USER));

        return userRepository.save(user);
    }

    /**
     * Deletes the user.
     *
     * @param user the user to delete
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    public User getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userRepository.findFirstByUsername(auth.getName());
    }

}
