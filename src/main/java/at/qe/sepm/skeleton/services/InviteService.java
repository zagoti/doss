package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Invite;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.Project;
import at.qe.sepm.skeleton.model.MemberRole;
import at.qe.sepm.skeleton.repositories.InviteRepository;
import at.qe.sepm.skeleton.repositories.UserRepository;
import at.qe.sepm.skeleton.services.MailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

@Component
@Scope("application")
public class InviteService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private InviteRepository inviteRepository;

	public void sendInvite(Project project, MemberRole role) {

		User user = userRepository.findFirstByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		Invite invite = new Invite();
		invite.setUser(user);
		invite.setProject(project);
		invite.setRole(role);

		inviteRepository.save(invite);

		MailService.sendMail("Einladung zur Projektteilnahme: \"" + project.getName() + "\" als " + role, "Hallo " + user.getFirstName() + " " + user.getLastName() + ",\ndu wurdest zum Projekt \"" + project.getName() + "\" als " + role + " eingeladen. Klicke auf <a href='/acceptinvite.xhtml?id=" + invite.getId() + "'>hier</a>, um die Einladung zu bestätigen!\n\nFreundliche Grüße\ntemp team", "flashfreezer@gmail.com"/*user.getMail()*/, "tempo@algebros.org", "crater.uberspace.de", "ImrDbSG6Qy9lgaRe");

	}
/*
	public void acceptInvite() {



	}
*/
}
