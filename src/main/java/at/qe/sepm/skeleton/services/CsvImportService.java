package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by mlex on 14.01.2017.
 */
@Component
@Scope("application")
public class CsvImportService {

    @Autowired
    UserService userService;

    /*
        Import users via csv files, the file should contain the following columns:
          - username
          - password
          - firstname
          - lastname
          - mail
          - role (if empty --> USER)

     */
    public void importUserFromCsv(MultipartFile file) throws IOException {
        InputStream inputStream = file.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            //split csv line
            String[] lineItems = line.split(";");

            //create user
            User usr = new User();
            usr.setUserName(removeWhitespaces(lineItems[0]));
            usr.setPassword(removeWhitespaces(lineItems[1]));
            usr.setFirstName(removeWhitespaces(lineItems[2]));
            usr.setLastName(removeWhitespaces(lineItems[3]));
            usr.setEmail(removeWhitespaces(lineItems[4]));
            Set<UserRole> roles = EnumSet.of(UserRole.USER);
            roles.add(UserRole.USER);

            //roles
            if(lineItems.length > 5){
                lineItems[5] = removeWhitespaces(lineItems[5]);
                if(lineItems[5].equals("Manager")) {
                    roles.add(UserRole.MANAGER);
                } else if(lineItems[5].equals("Admin")) {
                    roles.add(UserRole.MANAGER);
                    roles.add(UserRole.ADMIN);
                }
            }
            usr.setRoles(roles);

            //save user to db
            if (userService.saveUser(usr) != null)
                System.out.println("imported a user;");
            else
                System.out.println("user import failed");
        }
    }

    private String removeWhitespaces(String str){
        return str.replaceAll("\\s+","");
    }

    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
