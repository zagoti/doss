package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Time;
import at.qe.sepm.skeleton.model.Job;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

/**
 * Repository for managing {@link Time} entities.
 */
public interface TimeRepository extends AbstractRepository<Time, Long> {

	@Query("SELECT t FROM Time t WHERE id = :id")
    Time getTimeById(@Param("id") Long id);

	@Query("SELECT t FROM Time t WHERE jobId = :jobId AND fromtime > :fromtime AND totime < :totime")
	Collection<Time> findJobTimes(@Param("jobId") Long jobId, @Param("fromtime") Timestamp fromtime, @Param("totime") Timestamp totime);

	@Query("SELECT t FROM Time t WHERE username = :username AND fromtime > :fromtime AND totime < :totime")
	Collection<Time> findUserTimes(@Param("username") String username, @Param("fromtime") Timestamp fromtime, @Param("totime") Timestamp totime);

	@Query("SELECT t FROM Time t WHERE username = :username AND job.id = :jobId AND fromtime > :fromtime AND totime < :totime")
	Collection<Time> findUserJobTimes(@Param("username") String username, @Param("jobId") Long jobId, @Param("fromtime") Timestamp fromtime, @Param("totime") Timestamp totime);

	@Query("SELECT t FROM Time t WHERE username = :username")
	Collection<Time> getTimesForUser(@Param("username") String username);

	@Query("SELECT t FROM Time t WHERE (job IN :jobs) AND fromtime >= :lastweek")
	Collection<Time> getTimesOfUserOfJobsOfLastWeek(@Param("jobs") List<Job> jobs, @Param("lastweek") Timestamp lastWeek);

}
