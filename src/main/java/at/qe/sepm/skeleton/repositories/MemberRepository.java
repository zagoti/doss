package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.Member;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Repository for managing {@link Member} entities.
 */
public interface MemberRepository extends AbstractRepository<Member, Long> {

    @Query("SELECT m FROM Member m WHERE id = :id")
    Member getMemberById(@Param("id") Long id);
    
    @Query("SELECT m FROM Member m WHERE username = :username")
    List<Member> findByUser(@Param("username") String username);

    @Query("SELECT m FROM Member m WHERE username = :username AND projectId = :projectId")
    List<Member> findByUserInProject(@Param("username") String username, @Param("projectId") Long projectId);

    @Query("SELECT m FROM Member m WHERE projectId = :projectId")
    List<Member> findByProject(@Param("projectId") Long projectId);

    @Query("SELECT m FROM Member m WHERE :role IN role")
    List<Member> findByRole(@Param("role") String role);

    @Query("SELECT m.user FROM Member m WHERE projectId = :projectId AND 'LEADER' IN m.role")
    User getLeaderFromProject(@Param("projectId") Long projectId);

    @Query("SELECT (:role IN role) FROM Member WHERE id = :id")
    Boolean hasRole(@Param("id") Long id, @Param("role") String role);

}
