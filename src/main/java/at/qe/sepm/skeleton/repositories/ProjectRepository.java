package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.*;

/**
 * Repository for managing {@link Project} entities.
 */
public interface ProjectRepository extends AbstractRepository<Project, Long> {

    @Query("SELECT p FROM Project p")
    List<Project> getAll();

    @Query("SELECT name FROM Project p")
    List<String> getAllNames();

    @Query("SELECT p FROM Project p WHERE id = :id")
    Project getProjectById(@Param("id") Long id);

    @Query("SELECT p FROM Project p WHERE name = :name")
    Project findByName(@Param("name") String name);

    @Query("SELECT jobs FROM Project p WHERE p.id = :id")
    List<Job> getJobs(@Param("id") Long id);
    
    @Query("SELECT p FROM Project p WHERE p.id IN (SELECT DISTINCT project FROM Member WHERE username = :username)")
    List<Project> getProjectsOfLeader(@Param("username") String username);

    @Query("SELECT members FROM Project p WHERE p.id = :id")
    List<Member> getMembers(@Param("id") Long id);
    
    @Query("SELECT (:userId MEMBER OF members) FROM Project WHERE id = :id")
    Boolean isMember(@Param("id") Long id, @Param("userId") Long userId);

    @Query("SELECT (:jobId MEMBER OF jobs) FROM Project WHERE id = :id")
    Boolean hasJob(@Param("id") Long id, @Param("jobId") Long jobId);

    @Query("SELECT u FROM User u WHERE username NOT IN (SELECT user FROM Member WHERE project = :project)")
    Collection<User> getAllPossibleNewMembers(@Param("project") Project project);

}
