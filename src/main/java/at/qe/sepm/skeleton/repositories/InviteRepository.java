package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Invite;
import at.qe.sepm.skeleton.model.MemberRole;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for managing {@link Invite} entities.
 */
public interface InviteRepository extends AbstractRepository<Invite, Long> {

    @Query("SELECT i FROM Invite i WHERE id = :id")
    Invite getInviteById(@Param("id") Long id);

}
