package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.*;

import java.util.*;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for managing {@link User} entities.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
public interface UserRepository extends AbstractRepository<User, Long> {

    User findFirstByUsername(String username);

    List<User> findByUsernameContaining(String username);

    @Query("SELECT u FROM User u WHERE CONCAT(u.firstname, ' ', u.lastname) = :wholename")
    List<User> findByWholeNameConcat(@Param("wholename") String wholename);

    @Query("SELECT u FROM User u WHERE :role MEMBER OF u.roles")
    List<User> findByRole(@Param("role") UserRole role);
    
    @Query("SELECT u FROM User u WHERE username = :username")
    User getUserByName(@Param("username") String username);

}
