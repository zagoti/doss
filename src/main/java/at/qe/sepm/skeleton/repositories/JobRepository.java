package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.Job;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for managing {@link Job} entities.
 */
public interface JobRepository extends AbstractRepository<Job, Long> {

    @Query("SELECT j FROM Job j WHERE id = :id")
    Job getJobById(@Param("id") Long id);

    @Query("SELECT j FROM Job j WHERE projectid = :projectId")
    Collection<Job> findByProject(@Param("projectId") Long projectId);

    @Query("SELECT j FROM Job j WHERE name = :name")
    Job getJobByName(@Param("name") String name);

}
