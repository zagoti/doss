package at.qe.sepm.skeleton.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Entity representing members.
 */
@Entity
public class Member {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "username")
    private User user;
    @ManyToOne
    @JoinColumn(name = "projectid")
    private Project project;
    @Enumerated(EnumType.STRING)
    private MemberRole role;

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Project getProject() {
    	return project;
    }

    public MemberRole getRole() {
        return role;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setProject(Project project) {
    	this.project = project;
    }

    public void setRole(MemberRole role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        return 413 + Objects.hashCode(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Member))
            return false;

        final Member other = (Member) obj;
        if (!Objects.equals(this.id, other.id))
            return false;

        return true;
    }

    @Override
    public String toString() {
        if (user == null)
            return "<invalid user>";
        else
            return user.getUserName();
    }

}
