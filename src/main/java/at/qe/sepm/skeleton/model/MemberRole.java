package at.qe.sepm.skeleton.model;

/**
 * Enumeration of available member roles.
 */
public enum MemberRole {

    STUDENT,
    LEADER

}
