package at.qe.sepm.skeleton.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Entity representing times.
 */
@Entity
public class Time {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Timestamp fromtime; // don't use 'from', because jpa converts it to 'FROM *'
    private Timestamp totime;
    @ManyToOne
    @JoinColumn(name = "username")
    private User user;
    @ManyToOne
    @JoinColumn(name = "jobid")
    private Job job;

    public Long getId() {
        return id;
    }

    public Timestamp getFromTime() {
    	return fromtime;
    }

    public Timestamp getToTime() {
    	return totime;
    }

    public User getUser() {
        return user;
    }

    public Job getJob() {
    	return job;
    }

    public void setFromTime(Timestamp from) {
    	this.fromtime = from;
    }

    public void setToTime(Timestamp to) {
    	this.totime = to;
    }

    public void setUser(User user) {
    	this.user = user;
    }

    public void setJob(Job job) {
    	this.job = job;
    }

    @Override
    public int hashCode() {
        return 413 + Objects.hashCode(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Time))
            return false;

        final Time other = (Time) obj;
        if (!Objects.equals(this.id, other.id))
            return false;

        return true;
    }

    @Override
    public String toString() {
        return user + " stamped from " + fromtime + " to " + totime + ", by finishing " + job;
    }

}
