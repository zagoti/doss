package at.qe.sepm.skeleton.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

/**
 * Entity representing projects.
 */
@Entity
public class Project {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private String name;
    @Column(nullable = true)
    private String description;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "project", cascade = CascadeType.REMOVE)
    private Set<Job> jobs;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "project", cascade = CascadeType.REMOVE)
    private Set<Member> members;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
    	return description;
    }

    public Set<Job> getJobs() {
    	return jobs;
    }

    public Set<Member> getMembers() {
    	return members;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
    	this.description = description;
    }

    @Override
    public int hashCode() {
        return 413 + Objects.hashCode(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Project))
            return false;

        final Project other = (Project) obj;
        if (!Objects.equals(this.id, other.id))
            return false;

        return true;
    }

    @Override
    public String toString() {
        return name;
    }

}
