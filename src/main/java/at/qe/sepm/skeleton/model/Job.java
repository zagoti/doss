package at.qe.sepm.skeleton.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

/**
 * Entity representing jobs.
 */
@Entity
public class Job {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private String name;
    @Column(nullable = true)
    private String description;
    @ManyToOne
    @JoinColumn(name = "projectid")
    private Project project;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "job", cascade = CascadeType.REMOVE)
    private Set<Time> stamps;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
    	return description;
    }

    public Project getProject() {
        return project;
    }

    public Set<Time> getStamps() {
        return stamps;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
    	this.description = description;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public int hashCode() {
        return 413 + Objects.hashCode(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Job))
            return false;

        final Job other = (Job) obj;
        if (!Objects.equals(this.id, other.id))
            return false;

        return true;
    }

    @Override
    public String toString() {
        return name;
    }

}
