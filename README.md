# Tempo - Zeiterfassung für Projekte


1. [Über Tempo?](#tempo)
2. [Wie starte ich Tempo](#wie-starte-ich-tempo)
  * [direkt auf meinem PC?](#direkt-auf-meinem-pc)
  * [in einer Virtual Machine mittels Vagrant?](#virtual-machine-mit-vagrant)
3. [Tempo aufrufen](#tempo-aufrufen)
* [Login-Daten](#login-daten)

4. [Wie verwende ich Tempo](#wie-verwende-ich-tempo)
  * [als Admin](#als-admin)
  * [als Projektleiter](#als-leiter)
  * [als Student](#als-student)

----------


## Tempo
Tempo ist eine Software für Zeiterfassung von Studenten-Projektteams. Sie wurde im Rahmen des Proseminars "Entwurf von Software" im Wintersemester 2016 an der Universität Innsbruck entwickelt von

 - Maria Artho
 - Stephan Eberharter
 - Dominik Kuen
 - Alexander Leonhartsberger-Schrott
 - Matthias Lexer

Sie verwendet Spring boot, ist konfiguriert als Maven Web Anwendung und verwendet das Spring Framework, Tomcat mit JSF2-Support, eine H2 in-memory Datenbank, Primefaces, Bootstrap und FontAwesome Icons.


----------


## Wie starte ich Tempo

#### direkt auf meinem PC
Das Projekt herunterladen, via Terminal in den Projektordner navigieren und starten mittels

    mvn spring-boot:run
   Hierfür werden auf dem System Maven und Java 1.8 benötigt.
#### Virtual Machine mit Vagrant
(Getestet auf Windows 10, Fedora 25 und macOS Sierra) Das Projekt herunterladen, via Terminal in den Projektordner navigieren und starten mittels

    vagrant up
   Hierfür werden Vagrant und VirtualBox benötigt.


----------


## Tempo aufrufen
Nach dem Start von Tempo können Sie die Software benutzen, indem Sie - je nach Installationsweg - folgende URL in Ihrem Browser öffnen:

**bei direkter Installation:**
     http://localhost:8080

**bei Installation mittels Vagrant:**
     http://localhost:9000

Sie sollten danach auf der Login-Seite landen. 


----------
## Login Daten

#### User:

    admin
#### Passwort:

    passwd
----------

## Wie verwende ich Tempo

Nach dem Start der Anwendung steht Ihnen der **Admin Account** (Passwort: passwd) ebenso zur Verfügung wie ein **Demoprojekt**.


#### als Admin


Mit dem Admin Nutzer können Sie:

- Neue Projektleiter und Studenten hinzufügen
- Neue Projekte hinzufügen, Projekten Studenten und Leiter zuteilen. Leiter können mehrere Projekte leiten, aber Studenten lediglich einem Projekt zugeteilt werden.
- Jobs/Arbeitspakete für sämtliche Projekte verwalten.
- Studenten und Leiter mittels .csv-Import importieren. Achten Sie auf den [korrekten Aufbau der CSV](../user-upload-example.csv)

#### als Leiter

Mit einem Leiter können Sie:

- Statistiken über Ihre Projekte und Studenten erhalten
- Überblick über die erbrachten Stunden und die Aufteilung der Zeiten auf Arbeitspakete begutachten.
- Die Arbeitspakete Ihrer Gruppen bearbeiten

**_HINWEIS: Ein Leiter ist erst sinnvoll benutzbar, wenn er vom Admin einem Projekt zugewiesen wurde_**

#### als Student

Mit dem Studenten können Sie:

- Ein- und ausstempeln (wählen Sie zuerst ein Arbeitspaket aus. Sofern kein Arbeitspaket vorhanden ist, legen sie bitte mittels Leiter/Admin eines an.)
- Ihre eigene Gesamtzeit abfragen.
- Eine Auflistung Ihrer Arbeitszeiten erhalten.

**_HINWEIS: Ein Student ist erst sinnvoll, wenn er einem Projekt zugewiesen wurde und dieses Projekt einen Job vom Admin/Leiter beinhält_**